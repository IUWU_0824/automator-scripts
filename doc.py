import os
import tkinter as tk
import tkinter.ttk

testPlatFormEdit = ('荣耀Magic3 5.0.0.246(SP9ENTC143E136R4P12)',
                    '荣耀Magic4 6.0.0.203(SP6ENTC143E8R11P1)',
                    '荣耀Magic5 7.1.0.230(SP46ENTC143E29R4P1)',
                    '荣耀Magic5 7.1.0.230(SP40ENTC143E29R4P1)',
                    '荣耀Magic5 7.1.0.230(SP31ENTC143E29R4P1)',
                    '荣耀Magic5 7.1.0.230(SP27ENTC143E23R4P1)',
                    '荣耀Magic5 Pro 7.1.0.230(SP46ENTC143E29R4P1)',
                    '荣耀Magic5 Pro 7.1.0.230(SP40ENTC143E29R4P1)',
                    '荣耀Magic5 Pro 7.1.0.230(SP31ENTC143E29R4P1)',
                    '荣耀Magic5 Pro 7.1.0.230(SP27ENTC143E23R4P1)')

root = tk.Tk()
# root.iconbitmap('RC.ico')
root.attributes('-topmost', 1)
root.resizable(False, False)
root.geometry("360x720")
root.title("测试文档生成工具")

testMethodSelect = tk.IntVar()
testResult = tk.IntVar()
testRequest_1 = tk.IntVar()
testRequest_2 = tk.IntVar()
testRequest_3 = tk.IntVar()
testRequest_4 = tk.IntVar()
testRequest_5 = tk.IntVar()
testRequest_6 = tk.IntVar()
testRequest_7 = tk.IntVar()
testRequest_8 = tk.IntVar()
testRequest_9 = tk.IntVar()
testRequest_10 = tk.IntVar()
testRequest_11 = tk.IntVar()
testRequest_12 = tk.IntVar()
testRequest_13 = tk.IntVar()
# testRequest_14 = tk.IntVar()
# testRequest_15 = tk.IntVar()
# testRequest_16 = tk.IntVar()
# testRequest_17 = tk.IntVar()

Label_testVersion = tk.Label(root, text="测试版本：")
Label_testVersion.place(x=0, y=0)
Entry_testVersion = tk.Entry(root, width=40)
Entry_testVersion.place(x=60, y=0)

Label_platForm = tk.Label(root, text="测试平台：")
Label_platForm.place(x=0, y=30)
Combobox_platForm = tkinter.ttk.Combobox(root,
                                         width=40,
                                         values=testPlatFormEdit)
Combobox_platForm.place(x=60, y=30)

Label_testMethod = tk.Label(root, text="测试方式：")
Label_testMethod.place(x=0, y=60)
RadioButton_testMethod = tk.Radiobutton(root,
                                        text="线刷",
                                        variable=testMethodSelect,
                                        value=0)
RadioButton_testMethod.place(x=60, y=60)
RadioButton_testMethod_2 = tk.Radiobutton(root,
                                          text="OTA",
                                          variable=testMethodSelect,
                                          value=1)
RadioButton_testMethod_2.place(x=120, y=60)

Label_ztRequest = tk.Label(root, text="禅道需求：")
Label_ztRequest.place(x=0, y=90)
Entry_ztRequest = tk.Entry(root, width=40)
Entry_ztRequest.place(x=60, y=90)

Label_SVNAddress = tk.Label(root, text="定制需求：")
Label_SVNAddress.place(x=0, y=120)
Entry_SVNAddress = tk.Entry(root, width=40)
Entry_SVNAddress.place(x=60, y=120)

Label_testResult = tk.Label(root, text="测试结果：")
Label_testResult.place(x=0, y=150)
RadioButton_testResult = tk.Radiobutton(root,
                                        text="通过",
                                        variable=testResult,
                                        value=0)
RadioButton_testResult.place(x=60, y=150)
RadioButton_testResult_2 = tk.Radiobutton(root,
                                          text="不通过",
                                          variable=testResult,
                                          value=1)
RadioButton_testResult_2.place(x=120, y=150)

Label_testRequest_1 = tk.Label(root, text="应用程序安装受可信模块保护：")
Label_testRequest_1.place(x=0, y=180)
RadioButton_testRequest_1 = tk.Radiobutton(root,
                                           text="否",
                                           variable=testRequest_1,
                                           value=0)
RadioButton_testRequest_1.place(x=170, y=180)
RadioButton_testRequest_1_2 = tk.Radiobutton(root,
                                             text="是",
                                             variable=testRequest_1,
                                             value=1)
RadioButton_testRequest_1_2.place(x=270, y=180)

Label_testRequest_2 = tk.Label(root, text="工作区APN状态：")
Label_testRequest_2.place(x=0, y=210)
RadioButton_testRequest_2 = tk.Radiobutton(root,
                                           text="禁止增删改",
                                           variable=testRequest_2,
                                           value=0)
RadioButton_testRequest_2.place(x=170, y=210)
RadioButton_testRequest_2_2 = tk.Radiobutton(root,
                                             text="允许增删改",
                                             variable=testRequest_2,
                                             value=1)
RadioButton_testRequest_2_2.place(x=270, y=210)

Label_testRequest_3 = tk.Label(root, text="打开工作区禁用互联网机制：")
Label_testRequest_3.place(x=0, y=240)
RadioButton_testRequest_3 = tk.Radiobutton(root,
                                           text="是",
                                           variable=testRequest_3,
                                           value=0)
RadioButton_testRequest_3.place(x=170, y=240)
RadioButton_testRequest_3_2 = tk.Radiobutton(root,
                                             text="否",
                                             variable=testRequest_3,
                                             value=1)
RadioButton_testRequest_3_2.place(x=270, y=240)

Label_testRequest_4 = tk.Label(root, text="工作区5G网络模式定制：")
Label_testRequest_4.place(x=0, y=270)
RadioButton_testRequest_4 = tk.Radiobutton(root,
                                           text="自动",
                                           variable=testRequest_4,
                                           value=0)
RadioButton_testRequest_4.place(x=170, y=270)
RadioButton_testRequest_4_2 = tk.Radiobutton(root,
                                             text="NSA",
                                             variable=testRequest_4,
                                             value=1)
RadioButton_testRequest_4_2.place(x=220, y=270)
RadioButton_testRequest_4_3 = tk.Radiobutton(root,
                                             text="SA+NSA",
                                             variable=testRequest_4,
                                             value=2)
RadioButton_testRequest_4_3.place(x=270, y=270)

Label_testRequest_5 = tk.Label(root, text="开机默认进入工作区or生活区：")
Label_testRequest_5.place(x=0, y=300)
RadioButton_testRequest_5 = tk.Radiobutton(root,
                                           text="工作区",
                                           variable=testRequest_5,
                                           value=0)
RadioButton_testRequest_5.place(x=170, y=300)
RadioButton_testRequest_5_2 = tk.Radiobutton(root,
                                             text="生活区",
                                             variable=testRequest_5,
                                             value=1)
RadioButton_testRequest_5_2.place(x=270, y=300)

Label_testRequest_6 = tk.Label(root, text="开机动画定制：")
Label_testRequest_6.place(x=0, y=330)
RadioButton_testRequest_6 = tk.Radiobutton(root,
                                           text="默认",
                                           variable=testRequest_6,
                                           value=0)
RadioButton_testRequest_6.place(x=170, y=330)
RadioButton_testRequest_6_2 = tk.Radiobutton(root,
                                             text="定制",
                                             variable=testRequest_6,
                                             value=1)
RadioButton_testRequest_6_2.place(x=270, y=330)

Label_testRequest_7 = tk.Label(root, text="工作区锁屏壁纸：")
Label_testRequest_7.place(x=0, y=360)
RadioButton_testRequest_7 = tk.Radiobutton(root,
                                           text="默认",
                                           variable=testRequest_7,
                                           value=0)
RadioButton_testRequest_7.place(x=170, y=360)
RadioButton_testRequest_7_2 = tk.Radiobutton(root,
                                             text="定制",
                                             variable=testRequest_7,
                                             value=1)
RadioButton_testRequest_7_2.place(x=270, y=360)

Label_testRequest_8 = tk.Label(root, text="工作区桌面壁纸：")
Label_testRequest_8.place(x=0, y=390)
RadioButton_testRequest_8 = tk.Radiobutton(root,
                                           text="默认",
                                           variable=testRequest_8,
                                           value=0)
RadioButton_testRequest_8.place(x=170, y=390)
RadioButton_testRequest_8_2 = tk.Radiobutton(root,
                                             text="定制",
                                             variable=testRequest_8,
                                             value=1)
RadioButton_testRequest_8_2.place(x=270, y=390)

Label_testRequest_9 = tk.Label(root, text="工作区WIFI状态：")
Label_testRequest_9.place(x=0, y=420)
RadioButton_testRequest_9 = tk.Radiobutton(root,
                                           text="禁用",
                                           variable=testRequest_9,
                                           value=0)
RadioButton_testRequest_9.place(x=170, y=420)
RadioButton_testRequest_9_2 = tk.Radiobutton(root,
                                             text="启用",
                                             variable=testRequest_9,
                                             value=1)
RadioButton_testRequest_9_2.place(x=270, y=420)

Label_testRequest_10 = tk.Label(root, text="工作区蓝牙状态：")
Label_testRequest_10.place(x=0, y=450)
RadioButton_testRequest_10 = tk.Radiobutton(root,
                                            text="禁用",
                                            variable=testRequest_10,
                                            value=0)
RadioButton_testRequest_10.place(x=170, y=450)
RadioButton_testRequest_10_2 = tk.Radiobutton(root,
                                              text="启用",
                                              variable=testRequest_10,
                                              value=1)
RadioButton_testRequest_10_2.place(x=270, y=450)

Label_testRequest_11 = tk.Label(root, text="工作区NFC状态：")
Label_testRequest_11.place(x=0, y=480)
RadioButton_testRequest_11 = tk.Radiobutton(root,
                                            text="启用",
                                            variable=testRequest_11,
                                            value=0)
RadioButton_testRequest_11.place(x=170, y=480)
RadioButton_testRequest_11_2 = tk.Radiobutton(root,
                                              text="禁用",
                                              variable=testRequest_11,
                                              value=1)
RadioButton_testRequest_11_2.place(x=270, y=480)
    
Label_testRequest_12 = tk.Label(root, text="工作区定位服务状态：")
Label_testRequest_12.place(x=0, y=510)
RadioButton_testRequest_12 = tk.Radiobutton(root,
                                            text="启用",
                                            variable=testRequest_12,
                                            value=0)
RadioButton_testRequest_12.place(x=170, y=510)
RadioButton_testRequest_12_2 = tk.Radiobutton(root,
                                              text="禁用",
                                              variable=testRequest_12,
                                              value=1)
RadioButton_testRequest_12_2.place(x=270, y=510)

Label_testRequest_13 = tk.Label(root, text="工作区截屏、录屏状态：")
Label_testRequest_13.place(x=0, y=540)
RadioButton_testRequest_13 = tk.Radiobutton(root,
                                            text="禁用",
                                            variable=testRequest_13,
                                            value=0)
RadioButton_testRequest_13.place(x=170, y=540)
RadioButton_testRequest_13_2 = tk.Radiobutton(root,
                                              text="启用",
                                              variable=testRequest_13,
                                              value=1)
RadioButton_testRequest_13_2.place(x=270, y=540)

Label_testRequest_14 = tk.Label(root, text="预置APP：")
Label_testRequest_14.place(x=0, y=570)
Entry_testRequest_14 = tk.Entry(root, width=40)
Entry_testRequest_14.place(x=60, y=570)

Label_testRequest_15 = tk.Label(root, text="保活APP：")
Label_testRequest_15.place(x=0, y=600)
Entry_testRequest_15 = tk.Entry(root, width=40)
Entry_testRequest_15.place(x=60, y=600)

Label_testRequest_16 = tk.Label(root, text="防卸APP：")
Label_testRequest_16.place(x=0, y=630)
Entry_testRequest_16 = tk.Entry(root, width=40)
Entry_testRequest_16.place(x=60, y=630)

Label_testRequest_17 = tk.Label(root, text="预置APN：")
Label_testRequest_17.place(x=0, y=660)
Entry_testRequest_17 = tk.Entry(root, width=40)
Entry_testRequest_17.place(x=60, y=660)


def outputText():
    file = open('E:\\Document\\' + Entry_testVersion.get() + '.txt', "w")
    # file = open('C:\\thisBelongsToYourTool!\\result.txt')
    file.write('测试版本：' + Entry_testVersion.get() + '\n测试平台：' +
               Combobox_platForm.get() + '\n测试类型：')
    if testMethodSelect.get() == 0:
        file.write('线刷\n')
    elif testMethodSelect.get() == 1:
        file.write('OTA\n')
    file.write('禅道需求：' + Entry_ztRequest.get() + '\n定制需求：' +
               Entry_SVNAddress.get() + '\n------测试结果------\n')
    if testResult.get() == 0:
        file.write('完全符合定制需求\n')
    elif testResult.get() == 1:
        file.write('不符合定制需求\n')
    file.write('------测试细节------\n应用程序安装受可信模块保护：')
    if testRequest_1.get() == 0:
        file.write('否 ——通过')
    elif testRequest_1.get() == 1:
        file.write('是 ——通过')

    file.write('\n工作区APN状态：')
    if testRequest_2.get() == 0:
        file.write('（默认状态）禁止增删改 ——通过')
    elif testRequest_2.get() == 1:
        file.write('允许增删改 ——通过')

    file.write('\n打开工作区禁用互联网机制：')
    if testRequest_3.get() == 0:
        file.write('（默认状态）是 ——通过')
    elif testRequest_3.get() == 1:
        file.write('否 ——通过')

    file.write('\n工作区5G网络模式定制，默认自动：')
    if testRequest_4.get() == 0:
        file.write('（默认状态）自动 ——通过')
    elif testRequest_4.get() == 1:
        file.write('NSA ——通过')
    elif testRequest_4.get() == 2:
        file.write('SA+NSA ——通过')

    file.write('\n开机默认进入工作区or生活区：')
    if testRequest_5.get() == 0:
        file.write('（默认状态）开机进入工作区 ——通过')
    elif testRequest_5.get() == 1:
        file.write('开机进入生活区 ——通过')

    file.write('\n开机动画定制：')
    if testRequest_6.get() == 0:
        file.write('（默认状态）系统开机动画 ——通过')
    elif testRequest_6.get() == 1:
        file.write('定制开机动画 ——通过')

    file.write('\n工作区锁屏壁纸为通用默认状态：')
    if testRequest_7.get() == 0:
        file.write('（默认状态）默认工作区锁屏壁纸 ——通过')
    elif testRequest_7.get() == 1:
        file.write('定制工作区锁屏壁纸 ——通过')

    file.write('\n工作区桌面壁纸为通用默认状态：')
    if testRequest_8.get() == 0:
        file.write('（默认状态）默认工作区桌面壁纸 ——通过')
    elif testRequest_8.get() == 1:
        file.write('定制工作区桌面壁纸 ——通过')

    file.write('\n工作区WIFI状态：')
    if testRequest_9.get() == 0:
        file.write('（默认状态）禁用，用户无法打开 ——通过')
    elif testRequest_9.get() == 1:
        file.write('启用 ——通过')

    file.write('\n工作区蓝牙状态：')
    if testRequest_10.get() == 0:
        file.write('（默认状态）禁用，用户无法打开 ——通过')
    elif testRequest_10.get() == 1:
        file.write('启用 ——通过')

    file.write('\n工作区USB状态：（默认状态）禁用USB（仅充电） ——通过')

    file.write('\n工作区NFC状态：')
    if testRequest_11.get() == 0:
        file.write('（默认状态）打开，用户可打开和关闭 ——通过')
    elif testRequest_11.get() == 1:
        file.write('禁用，用户无法打开 ——通过')

    file.write('\n工作区定位服务状态：')
    if testRequest_12.get() == 0:
        file.write('（默认状态）打开，用户可打开和关闭 ——通过')
    elif testRequest_12.get() == 1:
        file.write('禁用，用户无法打开 ——通过')

    file.write('\n工作区截屏功能：')
    if testRequest_13.get() == 0:
        file.write('（默认状态）禁用 ——通过\n工作区录屏功能：（默认状态）禁用 ——通过')
    elif testRequest_13.get() == 1:
        file.write('启用 ——通过\n工作区录屏功能：启用 ——通过')

    file.write('\n生活区OTG状态：（默认状态）启用 ——通过\n工作区OTG状态：（默认状态）禁用 ——通过')

    file.write('\n预置APP：\n保活APP：\n不可卸载APP：\n自启APP：\n预置APN'
               '：')


Button_ok = tk.Button(root,
                      text="输出文档",
                      width=41,
                      command=lambda: outputText())
Button_ok.place(x=30, y=690)

root.mainloop()
