import tkinter
import tkinter.ttk
import os
import time
from tkinter import *
from tkinter.ttk import *
from scripts.system import reboot_recovery
from scripts.reports import output_logcat
from scripts.reports import output_report
from scripts.dependencies import add_bird_bro
from scripts.dependencies import add_fake_mdm
from scripts.dependencies import add_double_online
from scripts.dependencies import add_double_wlan

# Functions
def test_installed():
    var = entry.get()
    var_list = var.split()
    print('-----------------------\nTest Installed Apps Start')

    for i in range(len(var_list)):
        command = ("adb shell pm list packages | findstr" + " " + var_list[i])
        os.system(command)

    print('Test Installed Apps End\n-----------------------')


# adb shell am start -n 包名/类名 -a android.intent.action.MAIN -c android.intent.category.LAUNCHER
def test_auto_launch():
    var = entry.get()
    var_list = var.split()
    print('-----------------------\nTest Auto Launch Start')

    for i in range(len(var_list)):
        command = ("adb shell ps | findstr" + " " + var_list[i])
        os.system(command)

    print('Test Auto Launch End\n-----------------------')


def test_mechanism():
    var = entry.get()
    var_list = var.split()
    print('-----------------------\nTest Mechanism Start')

    for i in range(len(var_list)):
        command01 = ('adb shell ps | findstr' + " " + var_list[i])
        command02 = ('adb shell am force-stop' + " " + var_list[i])
        command03 = ('adb shell ps | findstr' + " " + var_list[i])
        os.system(command01)
        time.sleep(1)
        os.system(command02)
        time.sleep(1)
        os.system(command03)
        time.sleep(1)

    print('Test Mechanism End\n-----------------------')


def test_uninstall():
    var = entry.get()
    var_list = var.split()
    print('-----------------------\nTest Uninstall Start')

    for i in range(len(var_list)):
        command = ('adb uninstall' + " " + var_list[i])
        os.system(command)

    print('Test Uninstall End\n-----------------------')


# Create Window
mainWindow = tkinter.Tk()
mainWindow.title('AutomatorTools')
mainWindow.geometry('250x450+1300+300')
mainWindow.attributes('-topmost', 1)

# Buttons
addBirdBro = tkinter.Button(mainWindow, text='BirdBro', command=add_bird_bro)
addFakeMDM = tkinter.Button(mainWindow, text='FakeMDM', command=add_fake_mdm)
addDoubleOnline = tkinter.Button(mainWindow,
                                 text='AnnounceDoubleOnline',
                                 command=add_double_online)
addDoubleWlan = tkinter.Button(mainWindow,
                               text='NetworkDoubleOnline',
                               command=add_double_wlan)
rebootRecovery = tkinter.Button(mainWindow,
                                text='Reboot',
                                command=reboot_recovery)
testInstalled = tkinter.Button(mainWindow,
                               text='TestInstalled',
                               command=test_installed)
testUninstall = tkinter.Button(mainWindow,
                               text='TestUninstall',
                               command=test_uninstall)
testMechanism = tkinter.Button(mainWindow,
                               text='TestMechanism',
                               command=test_mechanism)
testAutoLaunch = tkinter.Button(mainWindow,
                                text='TestAutoLaunch',
                                command=test_auto_launch)
ouputLogcat = tkinter.Button(mainWindow, text='Logcat', command=output_logcat)
outputReport = tkinter.Button(mainWindow, text='Report', command=output_report)

# Entries
entry = tkinter.Entry(mainWindow, show=None)

# Layoutss
rebootRecovery.grid(row=0, column=0, sticky=N + W + W + E)
ouputLogcat.grid(row=1, column=0, sticky=N + W + W + E)
outputReport.grid(row=2, column=0, sticky=N + W + W + E)
addBirdBro.grid(row=3, column=0, sticky=N + W + W + E)
addFakeMDM.grid(row=4, column=0, sticky=N + W + W + E)
addDoubleOnline.grid(row=5, column=0, sticky=N + W + W + E)
addDoubleWlan.grid(row=6, column=0, sticky=N + W + W + E)
entry.grid(row=7, column=0, sticky=N + W + W + E, ipady=4)
testInstalled.grid(row=8, column=0, sticky=N + W + W + E)
testUninstall.grid(row=9, column=0, sticky=N + W + W + E)
testMechanism.grid(row=10, column=0, sticky=N + W + W + E)
testAutoLaunch.grid(row=11, column=0, sticky=N + W + W + E)

# MsgLoop
mainWindow.mainloop()
