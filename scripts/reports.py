import os
from datetime import datetime
import threading


def output_logcat():
    while True:
        command = ("adb logcat -b all > E:/Document/01-Logcat/" + "Logcat-" +
                   str(datetime.now().date()) + "-" +
                   str(datetime.now().time().hour) + "-" +
                   str(datetime.now().time().minute) + "-" +
                   str(datetime.now().time().second) + "-" +
                   str(datetime.now().time().microsecond) + ".txt")
        os.system(command)


# 233
# adb pull /storage/emulated/0/2023-10-27-13-38-34-log.zip E:\Document\04-Devicelog


def output_bugreport():
    print("Bugreport Output Started...")

    os.system("adb bugreport E:/Document/02-Bugreport")

    print("Bugreport Output End.")


def output_dropbox():
    print("Dropbox Output Started...")

    os.system(
        "adb shell dumpsys dropbox  --print --kx3 > E:/Document/03-Dropbox/Dropbox-"
        + str(datetime.now().date()) + "-" + str(datetime.now().time().hour) +
        "-" + str(datetime.now().time().minute) + "-" +
        str(datetime.now().time().second) + ".txt")

    print("Dropbox Output End.")


def output_report():
    threads = []
    t1 = threading.Thread(target=output_bugreport)
    threads.append(t1)
    t2 = threading.Thread(target=output_dropbox)
    threads.append(t2)
    for t in threads:
        t.start()
