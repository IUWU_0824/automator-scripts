import os


def add_bird_bro():
    os.system('adb install E:/Program/APK/util_apk/bird_bro.apk')


# 防止MDM霸屏
def add_fake_mdm():
    os.system(
        'adb install -r -d -t --user 10 --kx2 E:/Program/APK/util_apk/fake_mdm.apk'
    )
    os.system(
        'adb install -r -d -t --user 11 --kx2 E:/Program/APK/util_apk/fake_mdm.apk'
    )
    os.system('adb install -r -d -t E:/Program/APK/util_apk/fake_mdm.apk')


# 测试通知双在线的Demo
def add_double_online():
    os.system('adb install -r -d -t E:/Program/APK/util_apk/test_1.0.apk')


# 测试网络双在线
def add_double_wlan():
    os.system('adb install -r -d -t E:/Program/APK/util_apk/qq.apk')
    os.system('adb install -r -d -t --user 11 --kx2 E:/Program/APK/util_apk/qq.apk')
    os.system('adb install -r -d -t --user 10 --kx2 E:/Program/APK/util_apk/qq.apk')
